/*
 *  rhrdweb
 *
 *  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rhrdweb.
 *
 *  rhrdweb is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhrdweb is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with rhrdweb. If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

var current_week_offset = 0;

function getLastMonday() {
  var d = clock.now()
  d.setUTCHours(12, 0, 0, 0);
  var dow = d.getUTCDay()
  if(dow == 0) {
    return new Date(d.valueOf() - (6*24*3600000));
  } else {
    return new Date(d.valueOf() - ((dow-1)*24*3600000));
  }
}

function addDeltaDays(d, days) {
  return new Date(d.valueOf() + (days*24*3600000))
}

function isToday(d) {
  var today = clock.now()
  today.setUTCHours(12, 0, 0, 0);
  return ((d - today) == 0)
}

function addWeekClass(row, week) {
  switch(week) {
    case 1:
      row.addClass('label-info');
      break;
    case 2:
      row.addClass('label-warning');
      break;
    case 3:
      row.addClass('label-success');
      break;
    case 4:
      row.addClass('label-danger');
      break;
    default:
      row.addClass('label-default');
  }
}

function calendar_redraw(weekstart) {
  var cal = $('#calendar');
  cal.find("tr:gt(0)").remove();

  var date = getLastMonday();
  date = addDeltaDays(date, weekstart * 7);
  for(var w = 0; w < 20; w++) {
    var week = get_rd_week(date.valueOf() + 7199999);
    var row = $('<tr>');
    addWeekClass(row, week);
    row.append($('<td>').addClass('week').text(week));
    row.append($('<td>').addClass('month').text(monthname_short[date.getUTCMonth()] + ' ' + date.getUTCFullYear()));
    for(var d = 0; d < 7; d++) {
      var col = $('<td>').text(date.getUTCDate());
      if(isToday(date)) {
        col.addClass('today');
      }
      row.append(col);
      if(d < 6) {
          date = addDeltaDays(date, 1);
      }
    }
    row.append($('<td>').addClass('month').text(monthname_short[date.getUTCMonth()] + ' ' + date.getUTCFullYear()));
    date = addDeltaDays(date, 1);
    $('#calendar').append(row);
  }
}

function calendar_init() {
  $('#btn-today').click(calendar_today);
  $('#btn-earlier').click(calendar_prev);
  $('#btn-later').click(calendar_next);
  $('#calendar').on('wheel', calendar_scroll);
  current_week_offset = -4;
  clock.addCallback(function() {
    calendar_redraw(current_week_offset);
  });
}

function calendar_today() {
  current_week_offset = -4;
  calendar_redraw(current_week_offset)
}

function calendar_prev() {
  current_week_offset--;
  calendar_redraw(current_week_offset)
}

function calendar_next() {
  current_week_offset++;
  calendar_redraw(current_week_offset)
}

function calendar_scroll(event) {
  console.log(event.originalEvent.deltaY);
  if(event.originalEvent.deltaY > 0) {
    calendar_next();
    return false
  }

  if(event.originalEvent.deltaY < 0) {
    calendar_prev();
    return false
  }
  return false;
}
