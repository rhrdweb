/*
 *  rhrdweb
 *
 *  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rhrdweb.
 *
 *  rhrdweb is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhrdweb is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with rhrdweb. If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

var rh = rh || {};



/***************** models  *****************/

var shows = null;

rh.ShowList = function() {
  this.$this = $(this);
  this.shows = [];
  this.pv_names = {};
};

rh.ShowList.prototype.fetch = function() {
  var self = this;
  var yd = new Date((new Date()) - 24 * 3600 * 1000);
  var ydstr = yd.getFullYear() + '-' + Number(yd.getMonth() + 1).pad(2) + '-' + Number(yd.getDate()).pad(2);
  $.getJSON("/rh-bin/schedules.json?DAYS=5&START=" + ydstr, function(data) {
    if(data.status == "OK") {
      self.shows = [];
      $(data.shows).each(function(index, showdata) {
        var show = new rh.Show(showdata);
        if(self.shows.length > 0) {
          if(self.shows[self.shows.length-1].end > show.start) {
            self.shows[self.shows.length-1].end = show.start;
          }
        }
        self.shows.push(show);
      });
      self.$this.trigger('update');
    }
  });

  $.getJSON("/pv-export/timeslots_specials.json", function(data) {
    self.pv_names = {};
    $.each(data, function(specialid, specialdata) {
      if(specialdata.pv_id >= 0) {
        self.pv_names[specialdata.id] = specialdata.pv_name;
      }
    });
    self.$this.trigger('update');
  });
};

rh.Show = function(show) {
  this.id = show.id;
  this.title = show.title;
  this.start = new Date(show.start);
  this.len = show.len;
  this.end = new Date(this.start.valueOf() + show.len);
  this.pv_name = "";
};



/***************** views  *****************/

var showListView = null;

rh.ShowListView = function(model, maxlen) {
  this.model = model;
  this.maxlen = maxlen;

  this.showViews = [];

  var self = this;
  $(this.model).on('update', function() {
    self.render();
  });
  this.model.fetch();
};

rh.ShowListView.prototype.render = function() {
  var list = $('#shows');
  $('div.show', list).remove();
  this.showViews = [];

  var nowutc = clock.now();
  for (var i = 0; i < this.model.shows.length; i++) {
    var now = (nowutc.valueOf() + (this.model.shows[i].end.getTimezoneOffset() * 60000));
    if(now > this.model.shows[i].end) {
      continue;
    }

    this.model.shows[i].pv_name = this.model.pv_names[this.model.shows[i].id];
    var showView = new rh.ShowView(this.model.shows[i], this);
    this.showViews.push(showView);
    showView.render(now);
    list.append(showView.$el);
    if(this.showViews.length >= this.maxlen) {
      break;
    }
  }
};


rh.ShowView = function(model, listView) {
  this.model = model;
  this.listView = listView;

  this.$el = $('<div>');
};

rh.ShowView.prototype.render = function(now) {
  var startstr = "Beginn: " + format_time(this.model.start);
  var start = $('<span>').addClass('show-start').text(startstr);

  var endstr = "Live";
  if(this.model.len > 0) {
    endstr = "Ende: " + format_time(this.model.end);
  }
  var end = $('<span>').addClass('show-end').text(endstr);

  var t = this.model.title;
  if(typeof this.model.pv_name !== 'undefined' && this.model.pv_name != "") {
    t = this.model.pv_name;
  }
  var title = $('<span>').addClass('show-title').text(t);

  this.$el.empty().addClass('show').append(start).append(end).append(title);
  var until = this.model.start - now;
  if(until > 138000) {
    return
  }
  if(until > 123000) {
    if(until%2000 >= 1000) {
      this.$el.addClass('soon');
    }
    return
  }
  if(until > 30000) {
    this.$el.addClass('soon');
    return
  }
  if(until > 10000) {
    if(until%2000 >= 1000) {
      this.$el.addClass('imminent');
    }
    return
  }
  if(until > 0) {
    if(until%1000 >= 500) {
      this.$el.addClass('imminent');
    }
    return
  }
  this.$el.addClass('imminent');
}



/***************** controller  *****************/

function nextshows_init() {
  var url = parseLocationHref();
  var maxlen = 8;
  if(url.path.length > 1) {
    var tmp = parseInt(url.path[1], 10);
    if(!isNaN(tmp) && tmp > 0) {
      maxlen = tmp;
    }
  }
  shows = new rh.ShowList();
  showListView = new rh.ShowListView(shows, maxlen);

  setInterval("shows.fetch()", 5000);
  clock.addCallback(function() {
    showListView.render();
  });
}
