/*
 *  rhrdweb
 *
 *  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rhrdweb.
 *
 *  rhrdweb is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhrdweb is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with rhrdweb. If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

var rh = rh || {};



/***************** models  *****************/

var shows = null;

rh.ShowList = function() {
  this.$this = $(this);
  this.shows = [];
  this.request_sent = false;
};

rh.ShowList.prototype.fetch = function() {
  var self = this;

  this.request_sent = true;
  self.$this.trigger('pre-update');
  $.getJSON( "/pv-export/timeslots_specials.json", function(data) {
    self.shows = [];
    $.each(data, function(showid, showdata) {
      var show = new rh.Show(showdata);
      self.shows.push(show);
    });
    self.$this.trigger('update');
  }).fail(function() {
    alert("Error")
  }).always(function() {
    self.request_sent = false;
  });
};

rh.Show = function(show) {
  this.id = show.id;
  this.title = show.title.replace(" (Sondersendung)", "");

  this.pv_id = show.pv_id;
  this.pv_name = show.pv_name;
  this.pv_start = "??:??";
  this.pv_end = "??:??";

  if(typeof show.pv_start !== 'undefined') {
    this.pv_start = new Date(show.pv_start.replace("_", "T"));
  }
  if(typeof show.pv_end !== 'undefined') {
    this.pv_end = new Date(show.pv_end.replace("_", "T"));
  }
};



/***************** views  *****************/

var showListView = null;

rh.ShowListView = function(model) {
  this.model = model;

  this.showViews = [];

  var self = this;
  $(this.model).on('pre-update', function() {
    self.render_loading();
  });
  $(this.model).on('update', function() {
    self.render_list();
  });
  this.model.fetch();
};

rh.ShowListView.prototype.render_list = function() {
  $('#loading').hide();
  var list = $('#shows');
  $('div.show', list).remove();
  this.showViews = [];

  for (var i = 0; i < this.model.shows.length; i++) {
    var showView = new rh.ShowView(this.model.shows[i], this);
    this.showViews.push(showView);
    showView.render();
    list.append(showView.$el);
  }
  list.show();
};

rh.ShowListView.prototype.render_loading = function() {
  $('#shows').hide();
  $('#loading').show();
};


rh.ShowView = function(model, listView) {
  this.model = model;
  this.listView = listView;

  this.$el = $('<div>').addClass('row');
};

rh.ShowView.prototype.render = function() {
  var showlink = $('<a>').attr('href', "https://import.helsinki.at/shows/" + this.model.id)
                         .attr('target', "import").text(this.model.title);
  var title = $('<span>').addClass('show-title').append(showlink);
  var rd = $('<div>').addClass('col-md-3').append(title);

  var tag = $('<div>').addClass('col-md-1');
  var pv = $('<div>').addClass('col-md-8');
  if(this.model.pv_id >= 0) {
    var pvlink = $('<a>').attr('href', "https://pv.helsinki.at/admin/program/show/" + this.model.pv_id)
                         .attr('target', "import").text(this.model.pv_name);

    var pvtitle = $('<span>').addClass('pv-title').append(pvlink);

    var start_end = format_datetime(this.model.pv_start) + " - ";
    if(compare_date(this.model.pv_start, this.model.pv_end)) {
      start_end += format_time(this.model.pv_end);
    } else {
      start_end += format_datetime(this.model.pv_end);
    }
    var pvtimes = $('<span>').addClass('pv-time').text(start_end);

    pv.append(pvtitle).append(pvtimes);
    tag.append($('<div>').addClass('label label-danger').text("used"));
  } else {
    pv.text("-");
    tag.append($('<div>').addClass('label label-success').text("free"));
  }

  this.$el.empty().append(rd).append(tag).append(pv);
}



/***************** controller  *****************/

function specials_init() {
  shows = new rh.ShowList();
  showListView = new rh.ShowListView(shows);
}
