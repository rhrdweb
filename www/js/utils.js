/*
 *  rhrdweb
 *
 *  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rhrdweb.
 *
 *  rhrdweb is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhrdweb is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with rhrdweb. If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

function is_valid_date(d) {
  return (Object.prototype.toString.call(d) === '[object Date]') && (d.toString() !== 'Invalid Date')
}

Number.prototype.pad = function(size) {
  var s = String(this);
  while (s.length < (size || 2)) {s = "0" + s;}
  return s;
}

var weekday = new Array(7);
weekday[0] = 'Sonntag';
weekday[1] = 'Montag';
weekday[2] = 'Dienstag';
weekday[3] = 'Mittwoch';
weekday[4] = 'Donnerstag';
weekday[5] = 'Freitag';
weekday[6] = 'Samstag';

var weekday_short = new Array('So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa');

var monthname = new Array(7);
monthname[0] = 'Jänner';
monthname[1] = 'Februar';
monthname[2] = 'März';
monthname[3] = 'April';
monthname[4] = 'Mai';
monthname[5] = 'Juni';
monthname[6] = 'Juli';
monthname[7] = 'August';
monthname[8] = 'September';
monthname[9] = 'Oktober';
monthname[10] = 'November';
monthname[11] = 'Dezember';

var monthname_short = new Array('Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez');

function format_date_iso(d) {
  if(!is_valid_date(d)) {
    return '-';
  }
  return d.getFullYear() + '-' + Number(d.getMonth() + 1).pad(2) + '-' + Number(d.getDate()).pad(2);
}

function format_date(d) {
  if(!is_valid_date(d)) {
    return '-';
  }
  var datestr = weekday_short[d.getDay()];
  datestr += ' ' + Number(d.getDate()).pad(2);
  datestr += '.' + Number(d.getMonth() + 1).pad(2);
  datestr += '.' + d.getFullYear();
  return datestr;
}

function format_time(d) {
  if(!is_valid_date(d)) {
    return '-';
  }
  var timestr = Number(d.getHours()).pad(2);
  timestr += ':' + Number(d.getMinutes()).pad(2);
  timestr += ':' + Number(d.getSeconds()).pad(2);
  return timestr;
}

function format_datetime(d) {
  if(!is_valid_date(d)) {
    return '-';
  }
  var datetimestr = weekday_short[d.getDay()];
  datetimestr += ' ' + Number(d.getDate()).pad(2);
  datetimestr += '.' + Number(d.getMonth() + 1).pad(2);
  datetimestr += '.' + d.getFullYear();
  datetimestr += ' ' + Number(d.getHours()).pad(2);
  datetimestr += ':' + Number(d.getMinutes()).pad(2);
  datetimestr += ':' + Number(d.getSeconds()).pad(2);
  return datetimestr;
}

function format_durationms(time) {
  if(time == '-') return time;

  var h = Number(Math.floor(time / 3600000));
  time %= 3600000;
  var m = Number(Math.floor(time / 60000));
  time %= 60000;
  var s = Number(Math.floor(time / 1000));
  var hs = Number(Math.floor((time % 1000)/100));

  return h + ':' + m.pad(2) + ':' + s.pad(2) + '.' + hs;
}

function compare_date(date1, date2) {
  var d1 = new Date(date1).setHours(0,0,0,0);
  var d2 = new Date(date2).setHours(0,0,0,0);
  return d1 == d2;
}

function get_rd_week(msEpoch) {
  //
  // This computes the current Rivendell Week based on the number
  // of weeks since epoch.
  //
  // Explanation:
  //  epoch was at 01.01.1970 which was a Thursday.
  //  Monday in that week is (s-from-epoch + 3*24*60*60) seconds ago.
  //  This needs to be adjusted by the timezone offset for Europe/Vienna
  //  which is of course not constant (damn you daylight savings time)
  //  Divide this by (7*24*60*60) and you get the number of
  //  weeks since the Monday in the week of epoch adjusted for timezone offsets.
  //  This week had week number 3 so add an offset of 2 and
  //  get the modulo of 4. This rounded down gives you the current week
  //  with 0 meaning Week 1. So add 1 to that number and you will get
  //  the current RD week.
  //
  var sEpoch = msEpoch / 1000 ;
  var week = Math.floor((((sEpoch + 259200)/604800) + 2) % 4) + 1;
  return week;
}

function parseLocationHref() {
  var matches = window.location.href.match(/(https?):\/\/(.*)/);
  if(matches === null) {
    return null;
  }

  var uri = {};
  uri.scheme = matches[1];
  uri.servername = '';
  uri.path = [];
  uri.query = [];
  uri.fragment = '';

  if(matches[2].indexOf('/') < 0) {
    uri.servername = parts[2];
    return uri;
  }

  var parts = matches[2].split('/');
  uri.servername = parts[0];
  uri.path = parts.slice(1);

  var tmp = uri.path[uri.path.length-1];
  if(tmp.length > 0) {
    var qidx = tmp.indexOf('?');
    var fidx = tmp.indexOf('#');

    var query = '';
    if(qidx >= 0 && fidx >= 0) {
      uri.path[uri.path.length-1] = tmp.substring(0, qidx);
      if(qidx < fidx) {
        query = tmp.substring(qidx+1, fidx);
      }
      uri.fragment = tmp.substring(fidx+1);
    } else if (qidx >= 0 && fidx < 0) {
      uri.path[uri.path.length-1] = tmp.substring(0, qidx);
      query = tmp.substring(qidx+1);
    } else if (qidx < 0 && fidx >= 0) {
      uri.path[uri.path.length-1] = tmp.substring(0, fidx);
      uri.fragment = tmp.substring(fidx+1);
    }
    if(query.length > 0) {
      uri.query = query.split('&');
    }
  }

  return uri
}
