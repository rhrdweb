/*
 *  rhrdweb
 *
 *  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rhrdweb.
 *
 *  rhrdweb is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhrdweb is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with rhrdweb. If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

var rh = rh || {};



/***************** models  *****************/

var shows = null;

rh.ShowList = function(d) {
  this.$this = $(this);
  this.shows = [];
  if(!is_valid_date(d)) {
    this.current = new Date();
  } else {
    this.current = d;
  }
  this.current_week = get_rd_week(this.current.valueOf());
  this.request_sent = false;
};

rh.ShowList.prototype.fetch = function() {
  var self = this;
  this.current_week = get_rd_week(this.current.valueOf());

  this.request_sent = true;
  self.$this.trigger('pre-update');
  $.getJSON( "/rh-bin/schedules.json?DAYS=1&START=" + format_date_iso(this.current), function(data) {
    if(data.status == "OK") {
      history.replaceState(null, null, '/schedules/' + format_date_iso(self.current));
      self.last_succeeded = self.current;
      self.shows = [];
      $(data.shows).each(function(index, showdata) {
        var show = new rh.Show(showdata);
        self.shows.push(show);
      });
      self.$this.trigger('update');
    }
  }).fail(function() {
    if(is_valid_date(self.last_succeeded)) {
      self.current = self.last_succeeded;
      self.current_week = get_rd_week(self.current.valueOf());
      self.$this.trigger('update');
    }
  }).always(function() {
    self.request_sent = false;
  });
};

rh.ShowList.prototype.today = function() {
  if(!this.request_sent) {
    this.current = new Date();
    this.fetch();
  }
};

rh.ShowList.prototype.prev = function() {
  if(!this.request_sent) {
    this.current = new Date(this.current.valueOf() - 24*3600*1000);
    this.fetch();
  }
};

rh.ShowList.prototype.next = function() {
  if(!this.request_sent) {
    this.current = new Date(this.current.valueOf() + 24*3600*1000);
    this.fetch();
  }
};

rh.Show = function(show) {
  this.id = show.id;
  this.title = show.title;
  this.start = new Date(show.start);
  this.len = show.len;
};



/***************** views  *****************/

var showListView = null;

rh.ShowListView = function(model) {
  this.model = model;

  this.showViews = [];

  var self = this;
  $(this.model).on('pre-update', function() {
    self.render_hdr();
    self.render_loading();
  });
  $(this.model).on('update', function() {
    self.render_hdr();
    self.render_list();
  });
  this.model.fetch();
};

rh.ShowListView.prototype.render_hdr = function() {
  var hdr = $('#header');
  $('span.date', hdr).text(format_date(this.model.current));
  var weekspan = $('span.week', hdr).removeClass().addClass('week').addClass('label');
  switch(this.model.current_week) {
    case 1:
      weekspan.addClass('label-info').text('Woche 1');
      break;
    case 2:
      weekspan.addClass('label-warning').text('Woche 2');
      break;
    case 3:
      weekspan.addClass('label-success').text('Woche 3');
      break;
    case 4:
      weekspan.addClass('label-danger').text('Woche 4');
      break;
    default:
      weekspan.addClass('label-default').text('Fehler');
  }
};

rh.ShowListView.prototype.render_list = function() {
  $('#loading').hide();
  var list = $('#shows');
  $('div.show', list).remove();
  this.showViews = [];

  for (var i = 0; i < this.model.shows.length; i++) {
    var showView = new rh.ShowView(this.model.shows[i], this);
    this.showViews.push(showView);
    showView.render();
    list.append(showView.$el);
  }
  list.show();
};

rh.ShowListView.prototype.render_loading = function() {
  $('#shows').hide();
  $('#loading').show();
};


rh.ShowView = function(model, listView) {
  this.model = model;
  this.listView = listView;

  this.$el = $('<div>');
};

rh.ShowView.prototype.render = function() {
  var start = $('<span>').addClass('show-start').text(format_time(this.model.start));
  var lenstr = "-";
  if(this.model.len > 0) {
    lenstr = format_durationms(this.model.len);
  }
  var len = $('<span>').addClass('show-len').text("(" + lenstr + ")");

  var showlink = $('<a>').attr('href', "https://import.helsinki.at/shows/" + this.model.id)
                         .attr('target', "import").text(this.model.title);
  var title = $('<span>').addClass('show-title').append(showlink);

  this.$el.empty().addClass('show').append(start).append(title).append(len);
}



/***************** controller  *****************/

function schedules_init() {
  var url = parseLocationHref();
  var d = new Date();
  if(url.path.length > 1) {
    var tmp = new Date(url.path[1]);
    if(is_valid_date(tmp)) {
      d = tmp;
    }
  }
  history.pushState(null, null, '/schedules/' + format_date_iso(d));

  shows = new rh.ShowList(d);
  showListView = new rh.ShowListView(shows);

  $('#btn-today').click(schedules_today);
  $('#btn-earlier').click(schedules_prev);
  $('#btn-later').click(schedules_next);
}

function schedules_today() {
  shows.today();
}

function schedules_prev() {
  shows.prev();
}

function schedules_next() {
  shows.next();
}
