/*
 *  rhrdweb
 *
 *  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
 *
 *  This file is part of rhrdweb.
 *
 *  rhrdweb is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *
 *  rhrdweb is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with rhrdweb. If not, see <http://www.gnu.org/licenses/>.
 */

'use strict';

function Rhctl() {
  this.draw_callbacks = $.Callbacks('unique');
  this.state = 'NEW';

  this.sock_onmessage = function(event) {
    var msg = JSON.parse(event.data);
    if (msg.RESPONSE_CODE != 200) {
      console.log("got error message: ", event.data)
    } else {
      switch(msg.TYPE) {
      case 'state':
        this.draw_callbacks.fireWith(window, [ msg.STATE ]);
        break;
      case 'ack':
        break;
      default:
        console.log("rhctl: got unexpected message type '" + msg.TYPE + "'")
      }
    }
  }

  this.addCallback = function(cb) {
    this.draw_callbacks.add(cb);
  }

  this.sock_onopen = function() {
    this.state = 'CONNECTED';
    console.log('rhctl: ' + this.state)
    this.sock.onmessage = this.sock_onmessage.bind(this);
    this.sock.send(JSON.stringify({ COMMAND: 'subscribe', ARGS: [ 'state' ] }));
    this.sock.send(JSON.stringify({ COMMAND: 'state' }));
  }

  this.sock_onclose = function(event) {
    this.draw_callbacks.fireWith(window, []);
    this.sock.close();
    delete this.sock;
    setTimeout(this.connect.bind(this), 1000);
    this.state = 'RECONNECTING';
    console.log('rhctl: ' + this.state)
  }

  this.connect = function() {
    this.sock = new WebSocket('wss://' + window.location.host + '/rhctl/socket');
    this.sock.onopen = this.sock_onopen.bind(this);
    this.sock.onclose = this.sock_onclose.bind(this);
    this.state = 'CONNECTING';
    console.log('rhctl: ' + this.state)
  }
}

var rhctl = new Rhctl();

function rhctl_init() {
  rhctl.connect();
  rhctl_draw_state()
  rhctl.addCallback(rhctl_draw_state);
}

function rhctl_draw_state(state) {
  var raw = $('#rhctlraw');
  var mood = $('#rhctlmood img').removeClass();
  var srv = $('#rhctlactiveserver span').removeClass().addClass('label');
  var srvTable = $('#rhctlservertable table');
  srvTable.find("tr:gt(0)").remove();;
  var audioTable = $('#rhctlaudiotable table');
  audioTable.find("tr:gt(0)").remove();;

  if(!state) {
    raw.text("connecting...");
    mood.attr('src', '/img/mood-undefined.png');
    srv.addClass('label-default').text('none');
    return;
  }

    // Raw message
  raw.text(JSON.stringify(state, null, '\t'));

    // Mood
  mood.attr('src', '/img/mood-' + state.Mood + '.png');
  if(state.Settled) {
    mood.attr('title', state.Mood);
  } else {
    mood.addClass('blink').attr('title', state.Mood + ' (Settling)');
  }

    // Active Server
  srv.text(state.ActiveServer);
  switch(state.ActiveServer) {
  case 'master':
    srv.addClass('label-success');
    break;
  case 'standby':
    srv.addClass('label-warning');
    break;
  default:
    if(!state.ActiveServer) {
      srv.text('none');
    }
    srv.addClass('label-default');
  }

    // Server Table
  $.each(state.Server, function(key, value) {
     var entry = $('#hiddenTemplates .servertableEntryTemplate').clone().removeClass('servertableEntryTemplate');
      entry.find('.server-name').text(key);
      if(value.Channel) {
        entry.find('.server-channel').text(value.Channel);
      }
      var h = entry.find('.server-health img');
      if(value.ResurrectionLevel > 0 && value.ResurrectionLevel < 1) {
        h.addClass('blink').attr('src', '/img/server-resurrecting.png').attr('title', 'resurrecting');
      } else {
        switch(value.Health) {
        case 'alive':
        case 'dead':
          h.attr('src', '/img/server-' + value.Health + '.png').attr('title', value.Health);
          break;
        default:
        }
      }
      srvTable.append(entry)
  });


    // Audio Outputs
  $.each(state.Switch.Audio, function(index, value) {
     var entry = $('#hiddenTemplates .audiotableEntryTemplate').clone().removeClass('audiotableEntryTemplate');
     entry.find('.audio-out').text(index+1);
     $.each(value.Inputs, function(index, value) {
         if(value) {
             entry.find('.audio-in' + (index+1) + ' span').removeClass('glyphicon-unchecked').addClass('glyphicon-check');
         }
         });
     if(!value.Silence)  {
       entry.find('.audio-silence span').removeClass('glyphicon-volume-off').addClass('glyphicon-volume-up');
     }

     audioTable.append(entry)
  });
}
