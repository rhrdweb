LICENSE
=======

#
#  rhrdweb
#
#  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
#
#  This file is part of rhrdweb.
#
#  rhrdweb is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Affero General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  any later version.
#
#  rhrdweb is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Affero General Public License for more details.
#
#  You should have received a copy of the GNU Affero General Public License
#  along with rhrdweb. If not, see <http://www.gnu.org/licenses/>.
#

#
#  Some Icons (list below) are from http://www.flaticon.com/packs/emoticons-2
#  The license can be found in the file LICENSE-FlatIcon.pdf
#  List of Icons:
#   - www/img/mood-undefined.png
#   - www/img/mood-awakening.png
#   - www/img/mood-sad.png
#   - www/img/mood-nervous.png
#   - www/img/mood-happy.png
#   - www/img/server-undefined.png
#   - www/img/server-dead.png
#   - www/img/server-alive.png
#   - www/img/server-resurrecting.png
#

Installation
============

# sudo aptitude install apache2 libapache2-mpm-itk libapache2-mod-perl2 libjson-maybexs-perl librhrd-perl libdatetime-format-strptime-perl libjs-jquery
# sudo a2enmod ssl perl
# sudo /etc/init.d/apache2 restart

add the following to the virtualhost config:

~~~snip~~~
	AssignUserID rduser rivendell

	Include /etc/apache2/conf-available/javascript-common.conf

	Alias /rh-bin/ /var/www/rhrdweb/rh-bin/
	<Directory /var/www/rhrdweb/rh-bin/>
		SetHandler perl-script
		PerlResponseHandler ModPerl::Registry
		PerlOptions +ParseHeaders
		Options +ExecCGI

		Require all granted
	</Directory>

	ProxyPass "/ntp"           "ws://localhost:3000/ntp"
	ProxyPass "/rhctl/socket"  "ws://rhctl.helsinki.at:4080/socket"
	ProxyPass "/rhctl/"        "http://rhctl.helsinki.at:4080/"

	DocumentRoot /var/www/rhrdweb/www/
	<Directory /var/www/rhrdweb/>
		Options -Indexes -FollowSymLinks
		AllowOverride None

		Require all denied
	</Directory>
	<Directory /var/www/rhrdweb/www>
		Require all granted
	</Directory>
	AliasMatch "^/(js|img|styles)(.*)"  "/var/www/rhrdweb/www/$1$2"
	AliasMatch "^/([^./]+)(.*)"         "/var/www/rhrdweb/www/$1.html"
	ErrorDocument 404                   "/index.html"
~~~/snip~~~
